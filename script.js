var angularApp = angular.module('angularApp', []);

angularApp.controller('AppController', ['$scope', function($scope) {
    //$scope.message = "Hello, World!";
    $scope.removeFriend = function(friend) {
        var remove = $scope.information.indexOf(friend);
        $scope.information.splice(remove, 1);
    }
    $scope.addFriend = function() {
        $scope.information.push({
            name: $scope.newinfo.name,
            age: parseInt($scope.newinfo.age),
            available: false
        });
        $scope.newinfo.name = "";
        $scope.newinfo.age = ""
    };

    $scope.information = [
        {
            name: "Sahil",
            age: 22,
            available: false
        },
        {
            name: "Vansh",
            age: 19,
            available: false
        },
        {
            name: "Akash",
            age: 24,
            available: false
        },
        {
            name: "Harry",
            age: 25,
            available: false
        },
        {
            name: "Vishal",
            age: 27,
            available: false
        },
        {
            name: "Sunny",
            age: 20,
            available: false
        }
    ];
}]);